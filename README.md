# ACP controle times

Current version editor: Toby Wong, chunyauw @ uoregon.edu


## Description

This programme and web-based interface is based upton the ACP controle times.


The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

An example of a model based upon, would be the following calculator (https://rusa.org/octime_acp.html).


## Docker-Compose, AJAX, and Flask reimplementation

Difference between this to proj4-brevets, proj5 uses docker-compose instead of docker.

Functionalities to submit data to mongodb database, and ability to view the database is added.


## Execution

In order to work, please make sure your local disk have over 3GB of free space for MongoDB to run.

Shell scripts such as ./run.sh and ./rmi_all.sh are here to help you build, run, stop, delete docker images.

But be aware shell scripts run without sudo, considering you had added user to Docker-Group already.

For manual execution:

$run sudo docker-compose up


## File systems

Within the /brevets directory,

app.py is responsible for holding the web client.

acp_times.py is the local calculations units.


Within the /brevets/templates directory,

calc.html is the webpage to display user interface, that includes AJAX.

display.html displays the current database of mongodb

dserror.html displays error if database/submit entries are empty
